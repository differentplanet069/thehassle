<?php
require "bd.php";
require "admin/funcsAdmin.php";
?>
<html>
<head>
<link rel = "stylesheet" type = "text/css" href = "css/style/style.css"/>
    <style type="text/css">
        a
        {
            text-decoration: none;
            color: #161616;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
        a:hover
        {
            text-decoration: dotted;
            color: teal;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
    </style>
</head>
<body>
<a href="logout.php">Выход</a>
<?php
$prefix = '';
$logPass = '';
$data = $_POST;
$type = '';
if (isset($data['add']))
{
    $errors = array();
    $type = strtoupper($data['tableName']);
    if ($type == 'TEACHERS')
    {
        $prefix = 't_';
    }
    elseif ($type == 'STUDENTS')
    {
        $prefix = 's_';
    }
    $logPass = $prefix . generateUser($prefix);
    if (R::count($type, "LOGIN = ?", array($logPass))>0)
    {
        $errors[] = 'Пользователь с таким логином уже существует';
    }
    if (empty($errors))
    {
        $sql = "";
        if ($type == 'TEACHERS')
        {
            $sql = "INSERT INTO TEACHERS (`NAME`, `NAME_DAD`, `SURNAME`, `LOGIN`, `PASSWORD`) VALUES
                  ('$data[name]', '$data[middleName]', '$data[surname]',
                '$logPass', '".password_hash( $logPass, PASSWORD_DEFAULT)."');";
        }
        elseif ($type == 'STUDENTS')
        {
            $sql = "INSERT INTO STUDENTS (`NAME`, `SURNAME`, `LOGIN`, `PASSWORD`, `ID_CHARACTER`) VALUES
                  ('$data[name]', '$data[surname]',
                '$logPass', '".password_hash( $logPass, PASSWORD_DEFAULT)."', NULL);";
        }
        R::exec($sql);
        echo '<div class="justText" style="color: green; font-weight: bold;">'.'Добавлено'.'</div>';
    }
    else
    {
        echo '<div class="justText" style="color: red; font-weight: bold;">'.array_shift($errors).'</div>';
    }
}
?>
<form method="post" action="mainAdmin.php">
    <table align="center" cellspacing="10">
        <tr>
            <td class="justTextReg">INSERT INTO</td>
            <td><input type="text" name="tableName" maxlength="50" size="20"></td>
        </tr>
        <tr>
            <td class="justTextReg">NAME</td>
            <td><input type="text" name="name" size="20"></td>
        </tr>
        <tr>
            <td class="justTextReg">SURNAME</td>
            <td><input type="text" name="surname" size="20"></td>
        </tr>
        <tr>
            <td class="justTextReg">MIDDLE NAME</td>
            <td><input type="text" name="middleName" size="20"></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Добавить" class="justText" name="add"></td>
        </tr>
    </table>
</form>
</body>
</html>