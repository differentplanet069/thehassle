<?php
//require "bd.php";
require_once "funcs.php";
?>
<html>
<head>
    <title>
        Учитель | Результаты
    </title>
    <link rel = "stylesheet" type = "text/css" href = "/css/style.css"/>
    <style type="text/css">
        a
        {
            text-decoration: none;
            color: #161616;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
        a:hover
        {
            text-decoration: dotted;
            color: teal;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
    </style>
</head>
<body>
<p class="upText"> <a href="personalRoomTeacher.php">Личный кабинет</a> / <a href="logout.php">Выход</a></p>
<p class="header">Результаты тестов</p>
<?php
$obj = getUser();
$sqlSub = 'SELECT DISTINCT (SELECT SUBJECTS.NAME FROM SUBJECTS WHERE SUBJECTS.ID_SUBJECT = SUBJECT_TEACHERS.ID_SUBJECT)
        AS A FROM SUBJECT_TEACHERS WHERE SUBJECT_TEACHERS.ID_TEACHER = ' . $obj->{'ID_TEACHER'};
$rowsSub = R::getAll( $sqlSub );
$sqlCl = 'SELECT DISTINCT (SELECT CLASSES.LITER FROM CLASSES WHERE CLASSES.ID_CLASS = SUBJECT_TEACHERS.ID_CLASS)
        AS A FROM SUBJECT_TEACHERS WHERE SUBJECT_TEACHERS.ID_TEACHER = ' . $obj->{'ID_TEACHER'};
$rowsCl = R::getAll( $sqlCl );
?>
<form method="post" action="rezults.php">
    <table align="center" cellspacing="10">
        <tr>
            <td class="justTextReg">Предмет</td>
            <td>
                <select name="subject">
                    <option disabled>Выберите предмет</option>
                    <?php
                    echoArray($rowsSub);
                    ?>
                </select>
            </td>
        </tr>
        <tr>
            <td class="justTextReg">Класс</td>
            <td>
                <select name="subject">
                    <option disabled>Выберите класс</option>
                    <?php
                    echoArray($rowsCl);
                    ?>
                </select>
            </td>
        <tr>
            <td colspan="2" align="center"><input type="submit" value="Показать результаты" class="justText" name="log"></td>
        </tr>
    </table>
</form>
</body>
</html>
