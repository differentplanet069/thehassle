<?php
//require "bd.php";
require "funcs.php";
?>
<html>
<head>
    <title>
        Учитель | Личный кабинет
    </title>
    <link rel = "stylesheet" type = "text/css" href = "/css/style.css"/>
    <style type="text/css">
        a
        {
            text-decoration: none;
            color: #161616;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
        a:hover
        {
            text-decoration: dotted;
            color: teal;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
    </style>
</head>
<body>
<p class="upText"> <a href="index.php">Главная</a> / <a href="logout.php">Выход</a></p>
<p class="header">Личный кабинет</p>
<?php
$obj = getUser();
echo "<p class ='personalText'>".'Здравствуйте, ' . $obj->{'NAME'} . ' ' . $obj->{'NAME_DAD'} . '!'."</p>";?>
<p class ="personalText">Добро пожаловать в личный кабинет. Здесь вы можете:<br></p>
<pre>
    <a href="redpass.php" style="margin-left: 5%">&#9&#8226  Сменить пароль</a><br>
    <a href="rezults.php" style="margin-left: 5%">&#9&#8226  Просмотреть результаты тестов</a><br>
    <a href="t-tests.php" style="margin-left: 5%">&#9&#8226  Перейти к управлению тестами</a><br>
</pre>
</body>
</html>
