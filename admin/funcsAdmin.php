<?php
function getUser()
{
    $json = $_SESSION["logged"];
    $obj = json_decode($json);
    return $obj;
}

function generateUser($prefix)
{
    //prefix = 't_' or 's_'
    $sql = '';
    if ($prefix == 't_')
        $sql = 'SELECT MAX(ID_TEACHER) FROM TEACHERS;';
    elseif ($prefix == 's_')
        $sql = 'SELECT MAX(ID_STUDENT) FROM STUDENTS;';
    $rows = R::getAll( $sql );
    $count = '';
    foreach($rows as $item) {
        foreach ($item as $value) {
            $count .= $value;
        }
    }
    $countInt = intval($count) + 1;
    $count = strval($countInt);
    return str_pad($count, 10, "0", STR_PAD_LEFT);
}
?>