<?php
//require "bd.php";
require "funcs.php";
?>
<html>
<head>
    <title>
        Ученик | Личный кабинет
    </title>
    <link rel = "stylesheet" type = "text/css" href = "/css/style.css"/>
    <style type="text/css">
        a
        {
            text-decoration: none;
            color: #161616;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
        a:hover
        {
            text-decoration: dotted;
            color: teal;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
    </style>
</head>
<body>
<p class="upText"> <a href="index.php">Главная</a> / <a href="logout.php">Выход</a></p>
<p class="header">Личный кабинет</p>
<?php
$obj = getUser();
//fromArrayToString(getChapterDate($obj->{'ID_STUDENT'}));
if (isset($_GET['date']))
    echo "<p class =\"justText\">Сегодня " .date("d.m.Y") . "</p>";
my_calendar(array(date("Y-m-d")), array(getChapterDate($obj->{'ID_STUDENT'})));
echo "<p class ='personalText'>".'Привет, ' . $obj->{'NAME'} . '!'."</p>";?>
<p class ="personalText">Добро пожаловать в личный кабинет. Здесь ты можешь:<br></p>
<pre>
    <a href="redpass.php" style="margin-left: 5%">&#9&#8226  Сменить пароль</a><br>
    <a href="rezults.php" style="margin-left: 5%">&#9&#8226  Просмотреть свои предметы</a><br>
    <a href="balls.php" style="margin-left: 5%">&#9&#8226  Просмотреть свои результаты</a><br>
    <a href="t-tests.php" style="margin-left: 5%">&#9&#8226  Подготовиться к тестам</a><br>
</pre>
</body>
</html>