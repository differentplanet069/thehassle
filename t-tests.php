<?php
require "bd.php";
?>
<html>
<head>
    <title>
        Учитель | Управление тестами
    </title>
    <link rel = "stylesheet" type = "text/css" href = "/css/style.css"/>
    <style type="text/css">
        a
        {
            text-decoration: none;
            color: #161616;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
        a:hover
        {
            text-decoration: dotted;
            color: teal;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
    </style>
</head>
<body>
<p class="upText"> <a href="personalRoomTeacher.php">Личный кабинет</a> / <a href="logout.php">Выход</a></p>
<p class="header">Управление тестами</p>
<?php
$json = $_SESSION["logged"];
$obj = json_decode($json);
echo "<p class ='personalText'>".'Здравствуйте, ' . $obj->{'NAME'} . ' ' . $obj->{'NAME_DAD'} . '!'."</p>";?>
<p class ="personalText">Добро пожаловать в личный кабинет. Здесь вы можете:<br></p>
<pre>
    <a href="redinfo.php" style="margin-left: 5%">&#9&#8226  Просмотреть результаты тестов</a><br>
    <a href="createviz1.php" style="margin-left: 5%">&#9&#8226  Перейти к управлению тестами</a><br>
</pre>
</body>
</html>
