<?php
//require "bd.php";
require "funcs.php";
?>
<html>
<head>
    <link rel = "stylesheet" type = "text/css" href = "/css/style.css"/>
</head>
<body>
<?php
$data = $_POST;
$type = "";
if (isset($data['log']))
{
    $errors = array();
    if (substr($data['login'], 0, 2) == 't_')
    {
        $type = 'TEACHERS';
    }
    elseif (substr($data['login'], 0, 2) == 's_')
    {
        $type = 'STUDENTS';
    }
    else
    {
        $type = 'ADMIN';
    }
    $user = R::findOne($type, 'LOGIN = ?', [$data['login']]);
    $json = $user;
    $obj = json_decode($json);
    if ($user)
    {
        if (password_verify($data['password'], $obj->{'PASSWORD'}))
        {
            $_SESSION['logged'] = $user;
            if ($type == 'TEACHERS')
            {
                echo '<script>location.replace("personalRoomTeacher.php");</script>';
            }
            elseif ($type == 'STUDENTS')
            {
                echo '<script>location.replace("personalRoomStudent.php");</script>';
            }
            else
            {
                echo '<script>location.replace("mainAdmin.php");</script>';
            }
        }
        else
        {
            $errors[] = 'Неверный пароль';
        }
    }
    else
    {
        $errors[] = 'Пользователь с таким логином не существует';
    }
    if (! empty($errors))
    {
        echo '<div class="justText" style="color: red; font-weight: bold;">'.array_shift($errors).'</div>';
    }
}
?>
<form method="post" action="autorization.php">
    <table align="center" cellspacing="10">
        <tr>
            <td class="justTextReg">Логин</td>
            <td><input type="text" name="login" maxlength="50" size="20" required placeholder=""></td>
        </tr>
        <tr>
            <td class="justTextReg">Пароль</td>
            <td><input type="password" name="password" size="20" required placeholder=""></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Войти" class="justText" name="log"></td>
        </tr>
    </table>
</form>
</body>
</html>