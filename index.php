<?php
//require_once "bd.php";
require_once "funcs.php";
?>
<html>
<head>
    <link rel = "stylesheet" type = "text/css" href = "/css/style.css"/>
    <style type="text/css">
        a
        {
            text-decoration: none;
            color: #161616;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
        a:hover
        {
            text-decoration: dotted;
            color: teal;
            font-family: "Century Gothic";
            font-size: 17pt;
        }
    </style>
</head>
<body>
<?php
$obj = getUser();
if (isset($_SESSION['logged']))
{
    if (substr($obj->{'LOGIN'}, 0, 2) == "t_")
    {
        echo '<p class="upText"> <a href="personalRoomTeacher.php">Личный кабинет</a> / 
        <a href="logout.php">Выход</a></p>';
    }
    elseif (substr($obj->{'LOGIN'}, 0, 2) == "s_")
    {
        echo '<p class="upText"> <a href="personalRoomStudent.php">Личный кабинет</a>  / 
        <a href="logout.php">Выход</a></p>';
    }
}
else
{echo '<p class="upText"> <a href="autorization.php">Авторизация</a></p>';}
?>
<p class="header">The Hassle</p>
<div id="mainDescription" class="firstDiv">
    The Hassle — инновационная игра для учащихся школы. Это система, предназначенная для проверки знаний учеников.
    Помимо интересных заданий, в The Hassle существует также невероятный игровой мир. Вас ждут захватывающие приключения,
    необычные герои и просто приятное времяпрепровождение.
</div>
<div id="worldDescription">

</div>
<div id="chooseCharacter">

</div>
</body>
</html>