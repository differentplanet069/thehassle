<?php
//require "bd.php";
require "funcs.php";
?>
<html>
<head>
    <link rel = "stylesheet" type = "text/css" href = "/css/style.css"/>
</head>
<body>
<?php
$data = $_POST;
$obj = getUser();
$type = '';
if (substr($obj->{'LOGIN'}, 0, 2) == "t_")
    $type = 'TEACHERS';
elseif (substr($obj->{'LOGIN'}, 0, 2) == "s_")
    $type = 'STUDENTS';
if (isset($data['done']))
{
    $errors = array();
    if (! empty($data['passwordOld']) && ! empty($data['passwordNew']) && ! empty($data['passwordNew1']))
    {
        if (!password_verify($data['passwordOld'], $obj->{'PASSWORD'})) {
            $errors[] = 'Неправильно введен старый пароль';
        }
        if ($data['passwordNew1'] != $data['passwordNew']) {
            $errors[] = 'Неправильно введен повторный пароль';
        }
        $obj->{'PASSWORD'} = password_hash($data['passwordNew'], PASSWORD_DEFAULT);
        R::store($_SESSION['logged']);
    }
    if (! empty($errors))
    {
        echo '<div class="justText" style="color: red; font-weight: bold;">'.array_shift($errors).'</div>';
    }
    else
    {
        echo '<div class="justText" style="color: green; font-weight: bold;">'.'Информация успешно обновлена'.'</div>';
    }
}
?>
<form method="post" action="redpass.php">
    <table align="center" cellspacing="10">
        <tr>
            <td class="justTextReg">Старый пароль</td>
            <td><div class="control-group"><input type="password" name="passwordOld" ></div></td>
        </tr>
        <tr>
            <td class="justTextReg">Новый пароль</td>
            <td><div class="control-group"><input type="password" name="passwordNew" ></div></td>
        </tr>
        <tr>
            <td class="justTextReg">Подтвердите новый пароль</td>
            <td><div class="control-group"><input type="password" name="passwordNew1" ></div></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Готово" class="justText" name = "done"></td>
        </tr>
    </table>
</form>
</body>
</html>