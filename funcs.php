<?php
require "bd.php";
function getUser()
{
    $json = $_SESSION["logged"];
    $obj = json_decode($json);
    return $obj;
}

function echoArray($array)
{
    $outStr = "";
    foreach($array as $item) {
        foreach ($item as $value) {
            $outStr .= "<option value='" . $value . "'>" . "$value" . "</option>";
        }
        echo "<br />";
    }
    echo $outStr;
}

function getChapterDate($idStudent)
{
    //из таблицы chapter_progress выгружаем
    //id_chapter по id_student (вошедший юзер),
    //затем из таблицы chapters по id выгружаем дату
    $sql = 'SELECT DISTINCT (SELECT CHAPTERS.DATE FROM CHAPTERS
    WHERE CHAPTERS.ID_CHAPTER = CHAPTER_PROGRESS.ID_CHAPTER)
    AS A FROM CHAPTER_PROGRESS WHERE CHAPTER_PROGRESS.ID_STUDENT = ' . $idStudent . ';';
    $rows = R::getAll($sql);
    return $rows;
}

function fromArrayToString($array)
{
    foreach($array as $item) {
        foreach ($item as $value) {
            echo $value;
        }
    }
}

function my_calendar($fill = array(), $rows = array()) {
    $month_names=array("ЯНВАРЬ","ФЕВРАЛЬ","МАРТ","АПРЕЛЬ","МАЙ","ИЮНЬ",
        "ИЮЛЬ","АВГУСТ","СЕНТЯБРЬ","ОКТЯБРЬ","НОЯБРЬ","ДЕКАБРЬ");
    if (isset($_GET['y'])) $y=$_GET['y'];
    if (isset($_GET['m'])) $m=$_GET['m'];
    if (isset($_GET['date']) AND strstr($_GET['date'],"-")) list($y,$m)=explode("-",$_GET['date']);
    if (!isset($y) OR $y < 2018 OR $y > 2037) $y=date("Y");
    if (!isset($m) OR $m < 1 OR $m > 12) $m=date("m");

    $month_stamp=mktime(0,0,0,$m,1,$y);
    $day_count=date("t",$month_stamp);
    $weekday=date("w",$month_stamp);
    if ($weekday==0) $weekday=7;
    $start=-($weekday-2);
    $last=($day_count+$weekday-1) % 7;
    if ($last==0) $end=$day_count; else $end=$day_count+7-$last;
    $today=date("Y-m-d");
    $prev=date('?\m=m&\y=Y',mktime (0,0,0,$m-1,1,$y));
    $next=date('?\m=m&\y=Y',mktime (0,0,0,$m+1,1,$y));
    $i=0;
    ?>
    <table border=1 cellspacing=0 cellpadding=2>
        <tr>
            <td colspan=7>
                <table width="100%" border=0 cellspacing=0 cellpadding=0>
                    <tr>
                        <td align="left"><a href="<? echo $prev ?>">←</a></td>
                        <td align="center"><? echo $month_names[$m-1]," ",$y ?></td>
                        <td align="right"><a href="<? echo $next ?>">→</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td>Пн</td><td>Вт</td><td>Ср</td><td>Чт</td><td>Пт</td><td>Сб</td><td>Вс</td><tr>
            <?
            for($d=$start;$d<=$end;$d++) {
                if (!($i++ % 7)) echo " <tr>\n";
                echo '  <td align="center">';
                if ($d < 1 OR $d > $day_count) {
                    echo "&nbsp";
                } else {
                    $now="$y-$m-".sprintf("%02d",$d);
                    if (is_array($fill) AND in_array($now,$fill))
                    {
                        //echo '<b><a href="'.$_SERVER['PHP_SELF'].'?date='.$now.'">'.$d.'</a></b>';
                        echo '<b>'.$d.'</a></b>';
                    }
                    elseif (in_array(date_create_from_format("Y-m-d", $now), $rows))
                    {
                        echo '<b><a href="http://www.youtube.com">'.$d.'</a></b>';
                    }
                    else
                    {
                        echo $d;
                    }
                }
                echo "</td>\n";
                if (!($i % 7))  echo " </tr>\n";
            }
            ?>
    </table>
<? }
?>